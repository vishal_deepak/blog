class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy #Dependent destroy will destroy all comments that are related to this post
	validates_presence_of :title
	validates_presence_of :body

end
